package com.example.admin.mapapp;

import android.app.Activity;
import android.opengl.Matrix;
import android.support.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by Admin on 26.01.2018.
 */

public class FullSizeImageActivity extends Activity {
    Uri pathToPhoto;
    boolean mIsBtDelClick;
    private ScaleGestureDetector mScaleDetector;
    private int mAngle;
    private float mScale;
    private float StartScale;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_size_image);
        mIsBtDelClick = false;
        mScale = 1f;
        final ImageView imageView = findViewById(R.id.FullView);

        ImageButton btDel = findViewById(R.id.btDelImg);

        btDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsBtDelClick = true;
                getIntent().putExtra("result", mIsBtDelClick);
                setResult(Activity.RESULT_OK, getIntent());
                finish();
            }
        });

        if ( savedInstanceState == null){
            pathToPhoto = getIntent().getParcelableExtra("PhotoUri");
            mAngle = FixRotation( this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath() + '/' + pathToPhoto.getLastPathSegment());
        }
        else{
            pathToPhoto = savedInstanceState.getParcelable("PhotoRestore");
            mAngle = savedInstanceState.getInt("Angle");
        }

        Picasso.with(this)
                .load(pathToPhoto)
                .placeholder(android.R.drawable.ic_menu_gallery)
                .fit()
                .centerInside()
                .rotate(mAngle)
                .into(imageView);
        mScaleDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
            }
            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                StartScale = mScale;
                return true;
            }
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                    mScale = StartScale * detector.getScaleFactor();
                    mScale = Math.max(0.1f, Math.min(mScale, 5.0f));
                    imageView.setScaleX(mScale);
                    imageView.setScaleY(mScale);
                //}
                return false;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("PhotoRestore", pathToPhoto);
        outState.putInt("Angle", mAngle);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private int FixRotation(String mCurrentPhotoPath) {
        int rotationAngle;
        if (mCurrentPhotoPath == null) return 0;
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mCurrentPhotoPath);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        int orintation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        switch (orintation) {
            case ExifInterface.ORIENTATION_ROTATE_90: rotationAngle = 90; break;
            case ExifInterface.ORIENTATION_ROTATE_270: rotationAngle = 270;break;
            case ExifInterface.ORIENTATION_ROTATE_180: rotationAngle = 270; break;
            default: rotationAngle = 0;
        }
        return rotationAngle;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleDetector.onTouchEvent(event);
        return true;
    }
}
