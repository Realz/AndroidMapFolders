package com.example.admin.mapapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;

/**
 * Created by Admin on 25.02.2018.
 */

@Entity
public class MyImage {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "MarkerID")
    private int markerID;

    @ColumnInfo(name = "Path")
    private String path;

    public MyImage(){};

    public MyImage(int markerID, Uri uri){
        setUid(0);
        setMarkerID(markerID);
        setUri(uri);
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getMarkerID() {
        return markerID;
    }

    public void setMarkerID(int markerID) {
        this.markerID = markerID;
    }

    public void setUri(Uri uri){
        this.path = uri.toString();
    }

    public Uri getUri(){
        return Uri.parse(path);
    }
}
