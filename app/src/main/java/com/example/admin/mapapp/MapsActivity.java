package com.example.admin.mapapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;

    private List<MyMarker> mMarkers;
    private DataBase db;
    private int NextIndex;

    private NumberFormat mNumForm;
    static final int MARKER_CLICK_ACTION = 12842;
    static final int MY_LOCATION_REQUEST_CODE = 3354;

    /** Called when the user clicks a marker. */
    @Override
    public boolean onMarkerClick(final Marker marker) {
        Intent ClickEvent = new Intent(MapsActivity.this, Gallery.class);
        ClickEvent.putExtra("MarkerID", (int) marker.getTag());
        startActivityForResult(ClickEvent, MARKER_CLICK_ACTION);
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNumForm = NumberFormat.getNumberInstance();
        mNumForm.setMinimumFractionDigits(2);
        mNumForm.setMaximumFractionDigits(4);

        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(new OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(mNumForm.format
                        (latLng.latitude) + " : " + mNumForm.format(latLng.longitude));
                mMap.addMarker(markerOptions).setTag(NextIndex);
                MyMarker mm = new MyMarker(latLng, NextIndex++);

                // SAVED TO MARKER LIST
                new AsyncTask<MyMarker, Void, Void>() {
                    @Override
                    protected Void doInBackground(MyMarker... markers) {
                        db = Room.databaseBuilder(getApplicationContext(),
                                DataBase.class, "markers-database").build();
                        db.getMarkerDao().insertAll(markers);
                        db.close();
                        return null;
                    }
                }.execute(mm);
            }
        });

        // move the camera to Perm
        LatLng perm = new LatLng(58, 55);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(perm));
        initializeMarkers();
        CheckAndGetPermissions();
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MARKER_CLICK_ACTION) {
            if (resultCode == 10) {
                mMap.clear();
                initializeMarkers();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void initializeMarkers() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                db = Room.databaseBuilder(getApplicationContext(),
                        DataBase.class, "markers-database").build();
                mMarkers = db.getMarkerDao().getAllMarkers();
                NextIndex = mMarkers.size() > 0 ? db.getMarkerDao().getMaxID() + 1 : 1;
                db.close();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (mMarkers != null) {
                    for (MyMarker marker : mMarkers) {
                        mMap.addMarker(new MarkerOptions().position(marker.getLatLng())
                                .title(mNumForm.format(marker.getLat()) + " : " + mNumForm.format(marker.getLng())))
                                .setTag(marker.getUid());
                    }
                } else
                    mMarkers = new ArrayList<>();
            }
        }.execute();
    }

    @Override
    protected void onDestroy() {
        Intent intent = new Intent(this, NearbyService.class);
        stopService(intent);
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                    Intent intent = new Intent(this, NearbyService.class);
                    startService(intent);
                } else {
                    // Permission was denied. Display an error message.
                }
            }
        }
    }

    private void CheckAndGetPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            Intent intent = new Intent(this, NearbyService.class);
            startService(intent);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_LOCATION_REQUEST_CODE);
            }
        }
    }
}
