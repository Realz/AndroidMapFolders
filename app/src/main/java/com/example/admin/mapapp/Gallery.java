package com.example.admin.mapapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 16.01.2018.
 */

public class Gallery extends Activity{
    int mRemoveId;

    private int mCurMarkerID;
    private DataBase db;
    private List<MyImage> mPaths;
    private int width, height;

    ImageAdapter mImageAdapter;
    Uri mCurPhotoUri;
    GridView grid;




    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        mRemoveId = -1;

        setContentView(R.layout.gallerywindow);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        width = (int)(dm.widthPixels * .8);
        height = (int)(dm.heightPixels * .7);

        getWindow().setLayout(width,height);

        Button AddPhoto = findViewById(R.id.btAddPhoto);
        Button DeleteMarker = findViewById(R.id.btDelete);
        grid = findViewById(R.id.PhotoLayout);

        // TODO Check if I rotate screen, intent has previous value?
        mCurMarkerID = savedInstanceState == null ?
                getIntent().getIntExtra("MarkerID", -1) :
                savedInstanceState.getInt("MarkerFolderID", -1);

        initializeImages();
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent photoFullActivity = new Intent(Gallery.this, FullSizeImageActivity.class);
                mCurPhotoUri = mImageAdapter.mThumbIds.get(position).getUri();
                mCurrentPhotoPath = mCurPhotoUri.getPath();
                mRemoveId = position;
                photoFullActivity.putExtra("PhotoUri", mCurPhotoUri);
                startActivityForResult(photoFullActivity, REQUEST_VIEW_PHOTO);
            }
        });



        AddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });

        DeleteMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncTask<Integer, Void, Void>(){
                    @Override
                    protected Void doInBackground(Integer... markID) {
                        db = Room.databaseBuilder(getApplicationContext(),
                                DataBase.class, "markers-database").build();
                        File remove;
                        for (MyImage img : mPaths){
                            remove = new File(img.getUri().getPath());
                            remove.delete();
                        }
                        db.getImageDao().deleteByMarkerID(markID[0]);
                        db.getMarkerDao().deleteByID(markID[0]);
                        db.close();
                        return null;
                    }
                }.execute(mCurMarkerID);
                setResult(10);
                finish();
            }
        });
    }

    static final int REQUEST_TAKE_PHOTO = 12842;
    static final int REQUEST_VIEW_PHOTO = 54386;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                mCurPhotoUri = photoURI;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK && mCurPhotoUri != null) {
            new AsyncTask<Void, Void, Void>(){
                @Override
                protected Void doInBackground(Void... voids) {
                    MyImage mi = new MyImage(mCurMarkerID, mCurPhotoUri);
                    mPaths.add(mi);
                    db = Room.databaseBuilder(getApplicationContext(),
                            DataBase.class, "markers-database").build();
                    db.getImageDao().insertAll(mi);
                    db.close();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    ((ImageAdapter)grid.getAdapter()).notifyDataSetChanged();
                }
            }.execute();
        }
        if (requestCode == REQUEST_VIEW_PHOTO && resultCode == RESULT_OK && mRemoveId != -1) {
            if (data.getExtras().getBoolean("result", false)){
                new AsyncTask<Void, Void, Void>(){
                    @Override
                    protected Void doInBackground(Void... voids) {
                        File remove = new File(mCurrentPhotoPath);
                        remove.delete();
                        db = Room.databaseBuilder(getApplicationContext(),
                                DataBase.class, "markers-database").build();
                        db.getImageDao().delete(mPaths.get(mRemoveId));
                        db.close();
                        mPaths.remove(mRemoveId);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        ((ImageAdapter)grid.getAdapter()).notifyDataSetChanged();
                    }
                }.execute();
            }
        }
    }

    String mCurrentPhotoPath;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("PathToLastPhoto", mCurrentPhotoPath);
        outState.putInt("RemoveID", mRemoveId);
        outState.putInt("MarkerFolderID", mCurMarkerID);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentPhotoPath = savedInstanceState.getString("PathToLastPhoto");
        mRemoveId = savedInstanceState.getInt("RemoveID");
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @SuppressLint("StaticFieldLeak")
    private void initializeImages(){
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                db = Room.databaseBuilder(getApplicationContext(),
                        DataBase.class, "markers-database").build();
                mPaths = db.getImageDao().getMarkerImages(mCurMarkerID);
                db.close();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                mImageAdapter = new ImageAdapter(getBaseContext(), mPaths, width, height);
                grid.setAdapter(mImageAdapter);
            }
        }.execute();
    }

}
