package com.example.admin.mapapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;

/**
 * Created by Admin on 26.02.2018.
 */

public class NearbyService extends Service {

    private DataBase db;
    private LocationListener locationListener;
    private LocationManager locationManager;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        //throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("NearbyService", "onStart: " + intent);
        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);


        // Проверка разрешений
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                return START_NOT_STICKY;
            }
        }

        // Инициализация листерена
        locationListener = new LocationListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onLocationChanged(Location location) {
                Log.d("TEST", "############# onLocationChanged happened ############" +
                        "\n Latitude = "+location.getLatitude() + " Longitude = " + location.getLongitude());
                new AsyncTask<Location, Void, Void>(){
                    @Override
                    protected Void doInBackground(Location... location) {

                        db = Room.databaseBuilder(getApplicationContext(),
                                DataBase.class, "markers-database").build();
                        List<MyMarker> markers = db.getMarkerDao().getNearbyMarkers(location[0].getLatitude(), location[0].getLongitude());
                        db.close();
                        if (markers.size() > 0) {
                            MyMarker marker = markers.get(0);

                            Intent resultIntent = new Intent(NearbyService.this, Gallery.class);
                            resultIntent.putExtra("MarkerID", marker.getUid());
                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(NearbyService.this);
                            stackBuilder.addParentStack(Gallery.class);
                            stackBuilder.addNextIntent(resultIntent);

                            PendingIntent resultPendingIntent =
                                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(NearbyService.this, "SomeChannelID")
                                            .setSmallIcon(android.R.drawable.ic_dialog_map)
                                            .setContentTitle("Маркер рядом")
                                            .setContentText("Вы приблизились к маркеру " + marker.getUid())
                                            .setContentIntent(resultPendingIntent)
                                            .setAutoCancel(true);
                            NotificationManager mNotificationManager =
                                    (NotificationManager) getSystemService(
                                            Context.NOTIFICATION_SERVICE);
                            mNotificationManager.notify(1, mBuilder.build());
                        }

                        return null;
                    }
                }.execute(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("TEST", "############# onStatusChanged");
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d("TEST", "############# onProviderEnabled");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("TEST", "############# onProviderDisables");
            }
        };

        locationManager.removeUpdates(locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("DESTROY", "######### Service destroy happened ############");
        locationManager.removeUpdates(locationListener);
        super.onDestroy();
    }
}
