package com.example.admin.mapapp;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by Admin on 23.02.2018.
 */
@Database(entities = {MyMarker.class, MyImage.class /*, AThirdEntityType.class */}, version = 2)
public abstract class DataBase extends RoomDatabase {
    public abstract MarkerDAO getMarkerDao();
    public abstract ImageDAO getImageDao();
}
