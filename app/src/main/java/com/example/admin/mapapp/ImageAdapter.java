package com.example.admin.mapapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 26.01.2018.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    public List<MyImage> mThumbIds;
    private int size;

    public ImageAdapter(Context c, List<MyImage> Ids, int w, int h) {
        mContext = c;
        mThumbIds = Ids;
        size = w > h ? h : w;

    }

    public int getCount() {
        return mThumbIds != null ? mThumbIds.size() : 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(size/2,size/2));
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setPadding(5, 5, 5, 5);
        } else {
            imageView = (ImageView) convertView;
        }

        Picasso.with(mContext)
                .load(mThumbIds.get(position).getUri())
                .placeholder(android.R.drawable.ic_menu_gallery)
                .fit()
                .centerCrop()
                .rotate(FixRotation( mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath() + '/' + mThumbIds.get(position).getUri().getLastPathSegment()))
                .into(imageView);

        //Rotate(imageView, FixRotation( mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath() + '/' + mThumbIds.get(position).getLastPathSegment()));
        return imageView;
    }

    private int FixRotation(String mCurrentPhotoPath) {
        int rotationAngle;
        if (mCurrentPhotoPath == null) return 0;
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mCurrentPhotoPath);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        int orintation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        switch (orintation) {
            case ExifInterface.ORIENTATION_ROTATE_90: rotationAngle = 90; break;
            case ExifInterface.ORIENTATION_ROTATE_270: rotationAngle = 270;break;
            case ExifInterface.ORIENTATION_ROTATE_180: rotationAngle = 270; break;
            default: rotationAngle = 0;
        }
        return rotationAngle;
    }

    private void Rotate(ImageView imageView, int angle){
        if (angle == 0) return;
        Matrix matrix = new Matrix();
        imageView.setScaleType(ImageView.ScaleType.MATRIX);   //required
        matrix.postRotate((float) angle, imageView.getDrawable().getBounds().width()/2, imageView.getDrawable().getBounds().height()/2);
        imageView.setImageMatrix(matrix);
    }

    private static Bitmap ResizeImage(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth
                , height_tmp = o.outHeight;
        int scale = 1;

        while(true) {
            if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }
}
