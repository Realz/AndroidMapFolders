package com.example.admin.mapapp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Admin on 25.02.2018.
 */

@Dao
public interface ImageDAO {
    @Insert
    void insertAll(MyImage... images);

    @Delete
    void delete(MyImage image);

    @Query("DELETE FROM MyImage WHERE MarkerID = :idMarker")
    void deleteByMarkerID(Integer idMarker);

    @Query("SELECT uid, MarkerID, Path FROM MyImage")
    List<MyImage> getAllimages();

    @Query("SELECT uid, MarkerID, Path FROM MyImage WHERE MarkerID = :idMarker")
    List<MyImage> getMarkerImages(Integer idMarker);

    @Query("SELECT uid, MarkerID, Path FROM MyImage WHERE uid = :id")
    MyImage getImage(int id);
}
