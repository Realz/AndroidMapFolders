package com.example.admin.mapapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Admin on 23.02.2018.
 */

@Entity
public class MyMarker {
    @PrimaryKey
    private int uid;

    @ColumnInfo(name = "Latitude")
    private double lat;
    @ColumnInfo(name = "Longitude")
    private double lng;

    public MyMarker(){}

    public MyMarker(LatLng latLng, int index){
        lat = latLng.latitude;
        lng = latLng.longitude;
        uid = index;
    }

    public LatLng getLatLng(){
        return new LatLng(lat, lng);
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
