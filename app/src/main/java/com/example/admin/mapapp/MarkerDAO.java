package com.example.admin.mapapp;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Admin on 23.02.2018.
 */

@Dao
public interface MarkerDAO {
    @Insert
    void insertAll(MyMarker... markers);

    @Delete
    void delete(MyMarker marker);

    @Query("DELETE FROM MyMarker WHERE uid = :uid")
    void deleteByID(int uid);

    @Query("SELECT uid, Latitude, Longitude FROM MyMarker")
    List<MyMarker> getAllMarkers();

    @Query("SELECT MAX(uid) FROM MyMarker")
    int getMaxID();

    //@Query("SELECT uid, Latitude, Longitude FROM MyMarker WHERE ABS(Latitude - :lat) < 1 AND ABS(Longitude - :lng) < 1")
    @Query("SELECT uid, Latitude, Longitude FROM MyMarker " +
            "WHERE ABS(Latitude - :lat) < 1 AND ABS(Longitude - :lng) < 1 " +
            "GROUP BY uid " +
            "HAVING MIN(ABS(Latitude - :lat) + ABS(Longitude - :lng))")
    List<MyMarker> getNearbyMarkers(double lat, double lng);
}
